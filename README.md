# CWP Docker Compose based development environment.

## Requirements
 1. Docker
 2. Docker Compose
 3. Composer

## Installation Instructions
 1. Install Docker [https://www.docker.com/](https://www.docker.com/)
 2. Install Docker Compose [https://www.docker.com/products/docker-compose](https://www.docker.com/products/docker-compose)
 3. Install Composer [https://getcomposer.org/](https://getcomposer.org/doc/00-intro.md)
 4. Copy `docker-compose.yml` to your codebase
 5. Modify `docker-compose.yml` and give it a unique `container_name:`, e.g. container_name: my-website (defaults to 'web-cwp')
 6. Run `composer install` in your codebase
 7. Run `sudo docker-compose up` in your codebase

### Successful build
```
solr-cwp    | Solr is ready
web-cwp     | Webserver is ready
web-cwp     | http://{IP_ADDRESS}/
```

## Accessing your environment
 - **RUN** `sudo docker-compose exec {container_name} /bin/bash`
   (e.g. `sudo docker-compose exec web-cwp /bin/bash`)

This will give you bash access to the web environment at '/sites/cwp'. Within this directory you will find multiple directories to be used with your development environment
.

* **www/** - This is where you will find the local copy of your codebase. This is linked directly to your host so you should not need to modify this on the host.
* **logs/** - This is where you will find all the Apache error & access logs for the container you have connected to.
* **sspaks/** - This is a placeholder folder which you can upload snapshots to. This directory is persisted so all files should remain in between shutdown / startup.
* **_ss_environment.php** - This is the default environment file in which you will find your website constants defined.

## Using SSPak
- **Download:** [https://github.com/silverstripe/sspak](https://github.com/silverstripe/sspak)

>SSPak is a SilverStripe tool for managing database and assets content, for back-up, restoration, or transfer between environments.

SSPak is installed on the environment by default. With some quick simple steps, you can load / create / download SSPaks for your website.

### Upload a SSpak
If you have a SSPak file in which you would like to load onto your environment, you will first need to upload the file.

 `sudo docker cp {file} {container_name}:/sites/cwp/sspaks`

**E.g.** `user@host:~# sudo docker cp ~/downloads/my-sites-stuff.sspak my-site:/sites/cwp/sspaks/`

### Connect to Environment
First you will want to connect to your environment so you can run these commands locally. Refer to **Accessing your environment** above for how to connect to your environment.

`sudo docker-compose exec {container_name} /bin/bash`

**E.g.** `user@host:~/my-site# sudo docker-compose exec web-cwp /bin/bash`

_**NOTE:**_ Once you are connected to your environment you should be able to run SSPak commands as per normal.

_**NOTE:** _ All SSPak commands below should be run from the default bash directory '**/sites/cwp**'.

### Load a SSPak
If you loaded your SSPak using the instructions above, you will find your corresponding file in **'/sites/cwp/sspaks/'**.

`sspak load sspaks\{file} www/ && ./fix-permissions.sh`

##### Fix Permissions !IMPORTANT!
As the default user when you login is **root**, you will need to fix the permissions of folders / assets in order for certain features of SilverStripe to work after using SSpak.

You will find a **'fix-permissions.sh'** script in the main bash directory ( **/sites/cwp** ). running this script will fix up any bad permissions left lying around after executing your SSPak commands.

`./fix-permissions.sh`

**E.g.** `root@container_name:/sites/cwp# sspak load sspaks/my-sites-stuff.sspak www/ && ./fix-permissions.sh`

### Save an SSPak
The below instructions will create an SSPak of your environments website as per the [SSPak instructions](https://github.com/silverstripe/sspak/blob/master/README.md#use)

`sspak save www/ sspaks/{file}`

**E.g.** `root@container_name:/sites/cwp# sspak save www/ sspaks/my-sites-stuff.sspak`


### Download a SSPak
Once you have saved a SSPak of your environment, you can download and shared this file using the CP docker commands similar to **Upload a SSPak**.

 `sudo docker cp {container_name}:/sites/cwp/sspaks/{file} {local_location}`

**E.g.** `user@host:~# sudo docker cp my-site:/sites/cwp/sspaks/my-sites-stuff.sspak ~/sspaks/`

The above downloads the **my-sites-stuff.sspak** file created in the previous **Save a SSPak** steps to **~/sspaks/** on the host.
